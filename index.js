//Create a variable getCube and use the exponent operator to compute for the cube of a number.

const getCube = 2 ** 3

//using Templates Literals, print out the value of the getcube variable with a message of the cube of <num> is...
console.log (`The cube of 2 is ${getCube}`);

//Create a variable address with a value of an array containing details of an address
const address =[258, 'Washington Ave', 'NW', 'California', '90011'];
//Destracture the array and print out a message with the full address using Template Literals
const [houseNumber, street, city, state, postalCode] = address;
console.log(`I live at ${houseNumber} ${street} ${city}, ${state} ${postalCode}`);

//Create a variable animal with a value of an object data type with different animal details as its properties
const animalDetails = {
	name:'Lolong', 
	type: 'saltwater Crocodile', 
	weight: '1075 kgs', 
	height: '20 ft 3 in'
};

//Destracture the object and print out a message with the details of the animal using Template Literals
const {name, type, weight, height} = animalDetails;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${height}.`);


//Create an array of numbers
const numbers = [1, 2, 3, 4, 5];

//Loop through array using foreach, ann arrow function and using the implicit return statement to print out the numbers
numbers.forEach((numbers)=>{
	return console.log(numbers);
});


//Create a class of a Dog and constructor that will accept a name, age, and breed as its properties.
class Dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

//Create/ instantiate a new object from the class Dog and console log the object
const myDog = new Dog('Bantay', 2, 'German Shepherd')
console.log(myDog);
